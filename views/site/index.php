<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Felicidades</h1>

        <p class="lead">Aplicacion para la gestion de mis peliculas favoritas</p>
        <?= Html::img('@web/imgs/foto.jpg', ['alt' => 'Imgane principal']) ?>
    </div>
</div>
